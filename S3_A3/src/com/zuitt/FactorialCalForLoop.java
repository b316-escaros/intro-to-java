package com.zuitt;
import java.util.Scanner;

public class FactorialCalForLoop {
    public static void main(String[] args) {
        // Declare a Scanner object to read user input
        Scanner in = new Scanner(System.in);

        // Prompt the user to enter an integer
        System.out.println("Input an integer whose factorial will be computed: ");
        int num;
        try {
            num = in.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid input. Please enter an integer.");
            return;
        }

        // Initialize the answer variable
        int answer = 1;

        // Calculate the factorial using a for loop
        for (int counter = 1; counter <= num; counter++) {
            answer *= counter;
        }

        // Print the factorial
        System.out.println("The factorial of " + num + " is " + answer);
    }
}
