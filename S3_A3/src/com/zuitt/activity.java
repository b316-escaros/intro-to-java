package com.zuitt;

import java.util.Scanner;

public class activity {
    public static void main(String[] args) {
        String firstName, lastName;
        double firstSubject, secondSubject, thirdSubject;

        Scanner scanner = new Scanner (System.in);

        System.out.println("First Name: ");
        firstName = scanner.nextLine();

        System.out.println("Last Name: ");
        lastName = scanner.nextLine();

        System.out.println("First subject: ");
        firstSubject = scanner.nextDouble();

        System.out.println("Second subject: ");
        secondSubject = scanner.nextDouble();

        System.out.println("Third subject: ");
        thirdSubject = scanner.nextDouble();

        double averageGrade = (firstSubject + secondSubject + thirdSubject) / 3;

        System.out.println("Good day," + firstName + lastName + "Your average grade is " + averageGrade+"%" );

    }
}
