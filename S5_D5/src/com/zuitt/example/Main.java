package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello");

        Person person1 = new Person();
        person1.run();
        person1.sleep();

        StaticPoly staticPoly = new StaticPoly();
        System.out.println(staticPoly.addition(1, 5));
        System.out.println(staticPoly.addition(1, 5, 10));
        System.out.println(staticPoly.addition(15.5, 15.6));


        Parent parent1 = new Parent();
        parent1.speak();

        Child newChild = new Child();
        newChild.speak();
    }
}
