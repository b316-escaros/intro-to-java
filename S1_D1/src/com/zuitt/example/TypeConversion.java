package com.zuitt.example;
import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args) {
        Scanner myObj2 = new Scanner(System.in);
        System.out.println("How old are you?: ");
//Other way
//        double age = new Double(myObj2.nextLine());

        String age = myObj2.nextLine();

        double convert_age = Double.parseDouble(age);

        System.out.println("This is confirmation that you are " + age + " years old");

    }
}
