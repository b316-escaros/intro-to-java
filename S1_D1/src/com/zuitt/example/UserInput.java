package com.zuitt.example;
import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);//Create a scanner Object
        System.out.print("Enter a username ");

        String userName = myObj.nextLine();// Reads the user input
        System.out.println("Username is: " + userName);
    }
}
