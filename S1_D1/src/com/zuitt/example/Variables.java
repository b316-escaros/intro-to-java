package com.zuitt.example;

public class Variables {
    public static void main(String[] args) {
        // Variable declaration
        int age;
        char middle_name;

        // Variable declaration vs initialization
        int x;
        int y = 0;


        // Print the value of y
        System.out.println("The value of y is " + y);

    }
}
