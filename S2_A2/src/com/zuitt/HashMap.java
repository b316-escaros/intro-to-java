package com.zuitt;
public class HashMap {
    public static void main(String[] args) {

        // Create a HashMap of keys with data type String and values with data type Integer
        java.util.HashMap<String, Integer> inventory = new java.util.HashMap<>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        // Output the contents of the HashMap
        System.out.println("Our current inventory consists of: " + inventory);

    }
}
