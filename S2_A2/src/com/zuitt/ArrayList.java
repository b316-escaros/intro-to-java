package com.zuitt;

public class ArrayList {
        public static void main(String[] args) {
            // Create an ArrayList of String data-type elements
            java.util.ArrayList<String> friendsList = new java.util.ArrayList<>();

            friendsList.add("John");
            friendsList.add("Jane");
            friendsList.add("Chloe");
            friendsList.add("Zoey");

            // Output the contents of the ArrayList
            System.out.println("My friends are: " + friendsList);
        }
    }




