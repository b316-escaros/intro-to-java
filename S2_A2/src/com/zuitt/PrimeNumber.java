package com.zuitt;

public class PrimeNumber {
    public static void main(String[] args) {
        int[] primeNumbers = {2, 3, 5, 7, 11};

        // Output specific element
        System.out.println("The first prime number is: " + primeNumbers[0]);

    }
}
