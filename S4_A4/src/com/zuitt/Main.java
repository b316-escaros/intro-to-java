package com.zuitt;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        User user = new User("Oliver Queen", 38, "oliverqueen@mail.com", "Star City");

        Course course = new Course();
        course.setName("ARROW101");
        course.setDescription("An introduction of Archery");
        course.setSeats(12);
        course.setFee(8000.0);
        course.setStartDate(new Date());
        course.setEndDate(new Date());
        course.setInstructor(user);

        System.out.println("Hi! I'm " + user.getName() + ". I'm " + user.getAge() + " years old.");
        System.out.println("You can reach me via my email: " + user.getEmail() + ".");
        System.out.println("When I'm off work, I can be found at my house in " + user.getAddress() + ".");
        System.out.println();

        System.out.println("Welcome to the course " + course.getName() + ".");
        System.out.println("This course can be described as " + course.getDescription() + ".");
        System.out.println("The course has " + course.getSeats() + " seats available.");
        System.out.println("The course fee is $" + course.getFee() + ".");
        System.out.println("The course starts on " + course.getStartDate() + " and ends on " + course.getEndDate() + ".");
        System.out.println("Your instructor for this course is " + course.getInstructor().getName() + ".");
        System.out.println("Enjoy!");
    }
}
