package com.zuitt;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook(); // Instantiate a new Phonebook object

        Contact johnDoe = new Contact("John Doe", "+639152468596 +639228547963", "my home in Quezon City my office in Makati City");
        Contact janeDoe = new Contact("Jane Doe", "+639162148573 +639173698541", "my home in Caloocan City my office in Pasay City");

        phonebook.addContact(johnDoe);
        phonebook.addContact(janeDoe);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("The phonebook is empty.");// Output a notification message if the phonebook is empty
        } else {
            for (Contact contact : phonebook.getContacts()) {
                System.out.println(contact.getName()); // Output the contact's name
                System.out.println(contact.getName() + " has the following registered numbers: " + contact.getContactNumber());
                System.out.println(contact.getName() + " has the following registered addresses: " + contact.getAddress());
                System.out.println();
            }
        }
    }
}

