package com.zuitt;

public class Contact {
    private String name;
    private String contactNumber;
    private String address;

    public Contact() {// Default constructor for Contact class
    }

    public Contact(String name, String contactNumber, String address) {// Parameterized constructor for Contact class
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    // Getter and Setters method for contact
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}

