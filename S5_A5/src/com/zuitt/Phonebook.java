package com.zuitt;
import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts;

    public Phonebook() {
        contacts = new ArrayList<>();// Default constructor initializes contacts as an empty ArrayList
    }

    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;// Parameterized constructor for Phonebook class
    }
    // Getter and Setters method for contacts ArrayList
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }
    // Method to add a contact to the contacts ArrayList
    public void addContact(Contact contact) {
        contacts.add(contact);
    }
}
