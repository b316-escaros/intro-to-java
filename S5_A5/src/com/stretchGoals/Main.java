package com.stretchGoals;

public class Main {
    public static void main(String[] args) {
        // Instantiate a new User object
        User user = new User("Terence Gaffud", 25, "tgaff@mail.com", "Quezon City");
        // Print user details using getter methods
        System.out.println("Hi! I'm " + user.getName() + ". I'm " + user.getAge() + " years old. You can reach me via my email: " + user.getEmail() + ". When I'm off work, I can be found at my house in " + user.getAddress() + ".");

        // Instantiate a new Course object
        Course course = new Course();
        // Set course details using setter methods
        course.setName("MAC0004");
        course.setDescription("An introduction to Java for career-shifters");
        course.setSeats(30);
        course.setFee(199.99);
        course.setStartDate("2023-07-15");
        course.setEndDate("2023-09-15");
        course.setInstructor(user);
        // Print course details using getter methods
        System.out.println("Welcome to the course " + course.getName() + ". This course can be described as " + course.getDescription() + ". Your instructor for this course is Sir " + course.getInstructor().getName() + ". Enjoy!");
    }
}