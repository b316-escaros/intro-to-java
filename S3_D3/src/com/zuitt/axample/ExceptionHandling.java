package com.zuitt.axample;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);//The scanner object looks for which input to scan. In this case is the console(System.in)
        /*nextInt() - expects an integer
        * nextDouble() - expects a double
        * nextLine() - expects a line as a String
        * */
        System.out.println("Input a number: ");

        int num = 0;

        try{
            num = input.nextInt();//The nextInt() method tells Java that it is expecting an integer value as input.
        }catch(Exception e){
            System.out.println("Invalid input");
            e.printStackTrace();

        }
        System.out.println("You have entered: " + num);

    }
}
