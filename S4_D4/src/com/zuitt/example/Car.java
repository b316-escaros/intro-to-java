package com.zuitt.example;

public class Car {
    //Properties/Attributes - the characteristics of the object the class will create
    //Constructor - Method to create the object and instantiate with its initialize value
    //Getters and Setters - are methods to get the value of an object properties or set them
    //Methods - actions that an object can perform or do.

    //public access -  the variable/property in the class is accessible anywhere in the application
    //Private - limits the access and ability to get or set a variable/method to only within its own class
    //Setters - methods that allows us to set the value of a property
    private String make;

    private String brand;

    private int price;

    public Driver carDriver;

    // Constructor is method which allows us to set the initial value of an instance
    public Car() {
        this.carDriver = new Driver();
    }

    public Car(String make, String brand, int price, Driver driver) {
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }

    public String getMake() {
        return this.make;
    }

    public void setMake(String makeParams) {
        this.make = makeParams;
    }

    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String brandParams) {
        this.brand = brandParams;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int priceParams) {
        this.price = priceParams;
    }

    //Classes have relationship
    /*
     * Composition allows modelling objects to be made up of other objects. Classes can have instances of other classes
     * */
    public Driver getCarDriver() {
        return carDriver;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }

    //custom method to retrieve the car driver's name
    public String getCarDriverName() {
        return this.carDriver.getName();
    }
    public void start() {
        System.out.println("Vroom Vroom!!");
    }

}


