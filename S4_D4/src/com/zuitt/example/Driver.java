package com.zuitt.example;

public class Driver {
    public String name;
    private int age;

    public Driver(){

    }

    public Driver(String name, int age){
        this.name = name;
        this.age = age;
    }

    // Getters and setters for name and age

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
