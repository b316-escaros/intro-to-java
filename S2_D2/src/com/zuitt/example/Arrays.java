package com.zuitt.example;
import java.util.ArrayList;
import java.util.HashMap;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;

public class Arrays {
    public static void main(String[] args) {

    //    Declaration
    int[] intArray = new int[5];
//    the [] indicates that int data type should be able to hold multiple int values
//    "new" is a keyword used in non-primitive data types to tell Java to create said variable
//    This process is called "instantiation"
//    The integer value inside the [] on this side of the statement indicates the amount of integer values the array can hold

    // Declaration with initialization
    int[] intArray2 = {100, 200, 300, 400, 500};

    // Multi-dimensional Array
    String[][] classroom = new String[3][3];

// First Row
    classroom[0][0]="Athos";
    classroom[0][1]="Porthos";
    classroom[0][2]="Aramis";

// Second Row
    classroom[1][0] = "Brandon";
    classroom[1][1] = "JunJun";
    classroom[1][2] = "Jobert";

// Third Row
    classroom[2][0] = "Mickey";
    classroom[2][1] = "Donald";
    classroom[2][2] = "Goofy";

//        Accessing element in a Multi-dimensional Array
System.out.println(classroom[0][2]);


//        // Printing the classroom array
        for (int row = 0; row < classroom.length; row++) {
            for (int col = 0; col < classroom[row].length; col++) {
                System.out.print(classroom[row][col] + " ");
            }
            System.out.println(); // Move to the next line after each row
        }


        ArrayList<String> students = new ArrayList<String>();

//    Adding elements to the ArrayList

        students.add("John");
        students.add("Paul");

//        Accessing elements to the ArrayList
        students.get(0);
        System.out.println(students.get(1));

//        Updating an element of the ArrayList
        students.set(1, "George");
        System.out.println(students.get(1));

//        Removing a specific element in the ArrayList
        students.remove(1);
//        Removing all elements
        students.clear();

        System.out.println(students.size());

//        Hashmaps
        HashMap<String, String>job_position = new HashMap<String, String>();

//        Adding elements into the HashMap

        job_position.put("Brandon", "Student");
        job_position.put("Alice", "Dreamer");

//        Accessing elements of the HashMap
        job_position.get("Alice");
        System.out.println(job_position.get("Alice"));

//        Removing elements in HashMap

        job_position.remove("Brandon");
        System.out.println(job_position);

//        Getting the key of the elements of HashMaps
        System.out.println(job_position.keySet());

//        operators allows us to manipulate the values that we store in variables. They represent logical and arithmetic operations.
        /*
        * Types of Operators
        *
        * 1. Arithmetic: +,-,*,/,%
        * 2. comparison: >, <, <=, >=, ==, !=
        * 3. Logical: &&, ||, !
        * 4. Assignment: =
        *         * */

    }
}



